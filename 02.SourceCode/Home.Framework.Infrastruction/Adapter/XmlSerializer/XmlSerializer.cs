﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Home.Framework.Infrastructure
{
    /// <summary>
    /// C#对象XML序列化（一）：序列化方法和常用特性
    /// 参照连接：http://www.cnblogs.com/KeithWang/archive/2012/02/22/2363443.html
    /// </summary>
    public static class XmlSerializer
    {
        /// <summary>
        /// 类对象序列化为XML文件
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="filePath">The file path.</param>
        /// <param name="sourceObj">The source object.</param>
        /// <param name="xmlRootName">Name of the XML root.</param>
        public static void SaveToXml<T>(string filePath, object sourceObj, string xmlRootName)
        {
            Type type = typeof(T);
            try
            {
                if (!string.IsNullOrWhiteSpace(filePath) && sourceObj != null)
                {
                    FileUtils.CreateDirectoryIfNotExist(filePath);
                    type = type != null ? type : sourceObj.GetType();
                    using (StreamWriter writer = new StreamWriter(filePath))
                    {
                        System.Xml.Serialization.XmlSerializer xmlSerializer = null;
                        if (string.IsNullOrWhiteSpace(xmlRootName))
                        {
                            xmlSerializer = new System.Xml.Serialization.XmlSerializer(type);
                        }
                        else
                        {
                            xmlSerializer = new System.Xml.Serialization.XmlSerializer(type, new XmlRootAttribute(xmlRootName));
                        }
                        xmlSerializer.Serialize(writer, sourceObj);
                    }
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }

        public static T LoadFromXml<T>(string filePath)
        {
            if (File.Exists(filePath))
            {
                using (StreamReader reader = new StreamReader(filePath))
                {
                    System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                    return (T)xmlSerializer.Deserialize(reader);
                }
            }
            return default(T);
        }

        /// <summary>
        /// xml反序列化成实体类
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="strXml"></param>
        /// <returns></returns>
        public static T DeserializeFromXml<T>(string strXml)
        {
            try
            {
                using (var reader = new StringReader(strXml))
                {
                    System.Xml.Serialization.XmlSerializer xmlSerializer = new System.Xml.Serialization.XmlSerializer(typeof(T));
                    return (T)xmlSerializer.Deserialize(reader);
                }
            }
            catch (Exception ex)
            {
                Trace.WriteLine(ex.StackTrace);
                return default(T);
            }
        }
    }
}
