﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    public static class Converter
    {
        public static IDictionary<string, string> ToDictionary<T>(T target)
        {
            Type type = typeof(T);
            FieldInfo[] fields = type.GetFields(BindingFlags.NonPublic | BindingFlags.Instance | BindingFlags.Public);
            PropertyInfo[] properties = type.GetProperties();
            IDictionary<string, string> dictionary = new Dictionary<string, string>();
            object value = null;
            for (int i = 0; i < fields.Length; ++i)
            {
                value = fields[i].GetValue(target);
                if (value != null && !string.IsNullOrEmpty(value.ToString().Trim()))
                {
                    dictionary.Add(properties[i].Name, value.ToString());
                }
                else
                {
                    dictionary.Add(properties[i].Name, string.Empty);
                }
            }
            return dictionary;
        }
    }
}
