﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Home.Framework.Infrastructure
{
    public static partial class HttpUtils
    {
        /// <summary>
        /// HttpContext类型的扩展方法：解析HTTP上下文中的指定关键字
        /// </summary>
        /// <param name="context">HTTP上下文对象</param>
        /// <param name="key">要解析的关键字.</param>
        /// <returns>解析字符串</returns>
        public static string ParseContextKey(this HttpContext context, string key)
        {
            if (context == null || context.Items == null || context.Items[key] == null)
            {
                return string.Empty;
            }
            else
            {
                return context.Items[key].ToString();
            }
        }
    }
}
