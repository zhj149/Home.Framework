﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    public static partial class StringHelper
    {
        public static readonly char[] numberCharSet = new char[] { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };

        public static readonly char[] alphabetLowerCaseCharSet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z' };

        public static readonly char[] alphabetUpperCaseCharSet = new char[] { 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z' };

        public static readonly char[] alphabetCharSet = alphabetUpperCaseCharSet.Concat(alphabetLowerCaseCharSet).ToArray();

        public static string TrimCharSet(this string str, params char[] trimCharSet)
        {
            return string.Join(string.Empty, str.ToCharArray().Where(ch => !trimCharSet.Contains(ch)));
        }

        public static string TrimDigtal(this string str)
        {
            return TrimCharSet(str, numberCharSet);
        }

        public static string TrimLower(this string str)
        {
            return TrimCharSet(str, alphabetLowerCaseCharSet);
        }

        public static string TrimUpper(this string str)
        {
            return TrimCharSet(str, alphabetUpperCaseCharSet);
        }

        public static string TrimAlphabet(this string str)
        {
            return TrimCharSet(str, alphabetCharSet);
        }
    }
}
