﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    public static partial class StringHelper
    {
        /// <summary>
        /// 使用字符串作为字符串的分割依据
        /// </summary>
        /// <param name="str"></param>
        /// <param name="target"></param>
        /// <param name="option"></param>
        /// <returns></returns>
        public static string[] SplitByString(this string str, string target, RegexOptions option = RegexOptions.IgnoreCase)
        {
            return Regex.Split(str, target, option);
        }

        /// <summary>
        /// 字符串翻转
        /// </summary>
        /// <param name="str">The string.</param>
        /// <returns>System.String.</returns>
        public static string Reverse(this string str)
        {
            if(string.IsNullOrWhiteSpace(str))
            {
                return str;
            }
            else
            {       
                return string.Join(string.Empty, str.ToCharArray().Reverse());
            }
        }
    }
}
