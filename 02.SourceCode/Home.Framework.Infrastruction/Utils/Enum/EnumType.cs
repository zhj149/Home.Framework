﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    /// <summary>
    /// 返回状态枚举
    /// </summary>
    public enum ResponseState
    {
        异常 = -1,
        失败 = 0,
        成功 = 1,
    }
}
