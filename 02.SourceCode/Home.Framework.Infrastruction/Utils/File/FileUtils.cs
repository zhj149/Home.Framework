﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    public static class FileUtils
    {
        /// <summary>
        /// 判断路径是否存在，如果不存在的话则创建该目录
        /// </summary>
        /// <param name="filePath"></param>
        public static void CreateDirectoryIfNotExist(string filePath)
        {
            string dir = filePath.Substring(0, filePath.LastIndexOf("\\"));
            if (!Directory.Exists(dir))
            {
                Directory.CreateDirectory(dir);
            }
        }

        /// <summary>
        /// 根据文件路径名称计算文件MD5值
        /// 可能存在问题（比如不同目录下的同一个文件）
        /// 在网上搜索到相关问题之后考虑修正
        /// </summary>
        /// <param name="filePath">The file path.</param>
        /// <returns>System.String.</returns>
        public static string GetFileHashCode(string filePath)
        {
            return MD5Provider.GetFileHashCode(filePath);
        }
    }
}
