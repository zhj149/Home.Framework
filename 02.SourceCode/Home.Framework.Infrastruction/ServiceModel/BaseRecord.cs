﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    /// <summary>
    /// 返回的查询记录的基类
    /// </summary>
    /// <typeparam name="T">行记录类型</typeparam>
    public class BaseRecord<T> where T : class, new()
    {
        /// <summary>
        /// 构造函数，初始化结果集记录
        /// </summary>
        public BaseRecord()
		{
			Record = new List<T>();
		}

        /// <summary>
        /// 行记录结果集
        /// </summary>
        public List<T> Record { get; set; }
    }
}
