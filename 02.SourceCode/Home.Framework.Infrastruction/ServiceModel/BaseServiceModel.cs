﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    public abstract class BaseServiceModel
    {
        public BaseServiceModel()
        {
            ServiceSignature = new Signature();
            ServiceMessenger = new Messenger();
        }

        public Messenger ServiceMessenger { get; set; }

        public Signature ServiceSignature { get; set; }

        public TimeSpan ServiceInterval
        { 
            get
            {
                return this.ServiceMessenger.Interval;
            } 
        }

        public int ServiceState
        {
            get
            {
                return this.ServiceMessenger.State;
            }
        }

        public string ServiceStateName
        {
            get
            {
                return this.ServiceMessenger.StateName;
            }
        }
    }
}
