﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Home.Framework.Infrastructure
{
    public class Signature
    {
        public Signature()
        {
            this.SignInfo = string.Empty;
            this.SignType = string.Empty;
            this.CreateTime = DateTime.Now;
        }

        public string SignInfo { get; set; }

        public string SignType { get; set; }

        public DateTime CreateTime { get; set; }

    }
}
