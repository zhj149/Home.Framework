﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Home.Framework.Infrastructure
{
    public class ServiceMessage
    {
        public ServiceMessage()
        {
            this.Id = Guid.NewGuid().ToString("N");
            this.CreateTime = DateTime.Now;
            this.UpdateTime = this.CreateTime;
        }

        public string Id { get; set; }

        public string Message
        { 
            get 
            {
                if (this.MessageException != null)
                {
                    return this.MessageException.Message;
                }
                else if (this.MessageType != null)
                {
                    return EnumUtils.GetName(this.MessageType);
                }
                else
                {
                    return string.Empty;
                }
            } 
        }

        public string Sender { get; set; }

        public string Receiver { get; set; }

        public string Description { get; set; }

        protected Enum msgType { get; set; }

        public Enum MessageType 
        { 
            get
            {
                return this.msgType;
            }
            set
            {
                this.msgType = value;
                this.UpdateTime = DateTime.Now;
            }
        }

        protected Exception msgException { get; set; }

        public Exception MessageException
        {
            get
            {
                return this.msgException;
            }
            set
            {
                this.msgException = value;
                this.UpdateTime = DateTime.Now;
            }
        }

        public Type GetMessageType()
        {
            if (this.MessageException != null)
            {
                return this.MessageException.GetType();
            }
            else if (this.MessageType != null)
            {
                return this.MessageType.GetType();
            }
            else
            {
                return null;
            }
        }

        public DateTime CreateTime { get; private set; }

        public DateTime UpdateTime { get; private set; }

        public ResponseState ServiceState { get; set; }
    }
}
