﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Home.Framework.Infrastructure;
using System.Collections.Generic;
using System.Linq;

namespace Home.Framework.Test.Infrastruction.Utils
{
    [TestClass]
    public class DateTimeUtilsTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            // 获取当前时间
            DateTime target = DateTime.Now;

            // 判断当前日期是否为工作日
            bool isWorkDay = DateTimeUtils.IsWorkDay(target);

            // 工作日集合
            ISet<DateTime> workDays = DateTimeUtils.GetMonthWorkDays(target);

            // 第五工作日
            DateTime test = workDays.ToList()[4];

            // 当月第一天
            test = DateTimeUtils.FirstDayOfMonth(target);
            // 次月第一天
            test = DateTimeUtils.FirstDayOfNextMonth(target);
            // 当月最后一天
            test = DateTimeUtils.LastDayOfMonth(target);
        }
    }
}
