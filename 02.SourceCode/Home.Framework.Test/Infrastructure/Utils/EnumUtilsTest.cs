﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Home.Framework.Infrastructure;

namespace Home.Framework.Test.Infrastruction.Utils
{
    [TestClass]
    public class EnumUtilsTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            int defaultAccessMode = 1;
            bool flag = EnumUtils.ContainsValue<ResponseState>(defaultAccessMode);
            Assert.AreEqual(true, flag);

            defaultAccessMode = int.MinValue;
            flag = EnumUtils.ContainsValue<ResponseState>(defaultAccessMode);
            Assert.AreEqual(false, flag);
        }
    }
}
