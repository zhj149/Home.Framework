﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Home.Framework.Infrastructure;

namespace Home.Framework.Test.Infrastructure.Utils
{
    [TestClass]
    public class CommonUtilsTest
    {
        /// <summary>
        /// 传递到 ref 形参的实参必须先经过初始化，然后才能传递。
        /// 这与 out 形参不同，在传递之前，不需要显式初始化该形参的实参。
        /// </summary>
        [TestMethod]
        public void TestMethod1()
        {
            int a = 10;
            int b = 100;
            CommonUtils.Swap(ref a, ref b);
        }
    }
}
