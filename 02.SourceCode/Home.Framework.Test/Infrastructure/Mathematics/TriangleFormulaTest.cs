﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Home.Framework.Infrastructure;

namespace Home.Framework.Test.Infrastructure.Mathematics
{
    [TestClass]
    public class TriangleFormulaTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            double a = 3d;
            double b = 4d;
            double c = 5d;
            double s = TriangleFormula.HeronFormulaSquare(a, b, c);
            bool flag = s.Equals(6d);
            Assert.AreEqual(true,flag);

            a = 5d;
            b = 5d; 
            c = 6d;
            s = TriangleFormula.HeronFormulaSquare(a, b, c);
            flag = s.Equals(12d);
            Assert.AreEqual(true, flag);

            a = 3d;
            b = 4d;
            c = 5d;
            double r = TriangleFormula.CircumcircleRadius(a, b, c);
            flag = r.Equals(2.5d);
            Assert.AreEqual(true, flag);

            double x = Math.Sqrt(3d);
            a = 6d;
            b = 6d;
            c = 6d;
            r = TriangleFormula.IncircleRadius(a, b, c);
            flag = Math.Abs(r - x) < Math.Abs(double.MinValue);
            Assert.AreEqual(true, flag);

        }
    }
}
