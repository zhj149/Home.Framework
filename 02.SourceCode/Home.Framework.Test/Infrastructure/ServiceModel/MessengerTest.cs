﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Home.Framework.Infrastructure;

namespace Home.Framework.Test.Infrastruction.ServiceModel
{
    [TestClass]
    public class MessengerTest
    {
        [TestMethod]
        public void TestMethod1()
        {
            Messenger msg = new Messenger();

            // 不接受默认消息，必须通过赋值方法对其进行初始化
            //Assert.AreEqual(1, msg.State);
            //Assert.AreEqual(string.Empty, msg.ServiceMsg.Message);

            msg.ReceiveMessage(ResponseState.成功);
            Assert.AreEqual(1, msg.State);
            Assert.AreEqual(ResponseState.成功.ToString(), msg.StateName);

            msg.ReceiveError(ResponseState.失败);
            Assert.AreEqual(0, msg.State);
            Assert.AreEqual(ResponseState.失败.ToString(), msg.StateName);

            string exceptionMsg = "索引值超出范围";
            Exception exp = new ArgumentOutOfRangeException(exceptionMsg);
            msg.ReceiveException(exp);
            Assert.AreEqual(-1, msg.State);
            Assert.AreEqual(ResponseState.异常.ToString(), msg.StateName);
            Assert.AreEqual(exp, msg.ServiceMsg.MessageException);
        }
    }
}
